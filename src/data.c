#include "data.h"

/* Determines the index based on the offset from the array address. */
#define DERIVE_ID(base, cur, type) (u32)(((u8 *)cur - (u8 *)base) / sizeof(type))

/* Returns true if a string has lowercase characters. */
static bool has_lowercase(char *str) {
    for(; *str != '\0'; ++str) {
        if(*str >= 'a' && *str <= 'z') {
            return true;
        }
    }

    return false;
}

/* Clears stdin line. */
static void clear_stdin() {
    int c;
    while((c = getchar()) != '\n' && c != EOF);
}

/* Get the index of an activity, returns the arg index (limit) if it fails. */
static int get_activity_index(activity_t *array, const activity_t match, const int index) {
    int i;
    for(i = 0; i < index; ++i) {
        if(!strcmp(array[i], match))
            break;
    }

    return i;
}

/* Returns true if a user exists. */
static bool user_exists(user_t *array, const user_t match, const int index) {
    int i;
    for(i = 0; i < index; ++i) {
        if(!strcmp(array[i], match))
            return true;
    }

    return false;
}

/* Returns true if a certain description is already in a task. */
static bool task_duplicate_description(task_t *tasks, const char *desc, int index) {
    int i;
    for(i = 0; i < index; ++i) {
        if(!strcmp(tasks[i].desc, desc))
            return true;
    }

    return false;
}

/* Fgets the string after space characters and nulls where '\n' is. */
/* 1st char is read with getchar. */
static void get_after_space(char *str, u32 lim) {
    /* To allow an aribitrary number of spaces. */
    while(isspace(((u8 *)str)[0] = getchar()));
    fgets(str + 1, lim-1, stdin);
    str[strlen(str) - 1] = '\0';
}

/* Checks if there is any text. If it reads the current line, it lists and returns. */
/* If it is found, it saves the first char in usr. */
static bool user_try_list_is_empty_stdin(user_t usr, int index, user_t *data) {
    int c;
    while(isspace(c = getchar())) {
        if(c == '\n' || c == EOF) {
            int i;
            for(i = 0; i < index; ++i)
                puts(data[i]);
            return true;
        }
    }
    ((u8 *)usr)[0] = (u8)c;
    return false;
}

/* Checks if there is any text. If it reads the current line, it lists and returns. */
/* If it is found, it saves the first char in act. */
static bool activity_try_list_is_empty_stdin(activity_t act, int index, activity_t *data) {
    int c;
    while(isspace(c = getchar())) {
        if(c == '\n' || c == EOF) {
            int i;
            for(i = 0; i < index; ++i)
                puts(data[i]);
            return true;
        }
    }
    ((u8 *)act)[0] = (u8)c;
    return false;
}

/* Swaps 2 tasks. */
static void swap_task(task_t **a, task_t **b) {
    task_t *tmp = *a;
    *a = *b;
    *b = tmp;
}

/* Strcmp between descriptions. */
static int task_strcmp(task_t *a, task_t *b) {
    return strcmp(a->desc, b->desc);
}

/* Compare start values.*/
static int task_numcmp(task_t *a, task_t *b) {
    int ret = a->start - b->start;
    if(!ret)
        ret = strcmp(a->desc, b->desc);
    return ret;
}

/* Decides the partion index to split the current sorting group. */
static u32 decide_partition_task(task_t **tasks, u32 task_amount, int (*check)(task_t *, task_t *))
{
    task_t * const last = tasks[task_amount - 1];
    u32 i, small = 0;

    for(i = 0; i < task_amount - 1; ++i)
        if(check(tasks[i], last) < 0)
            swap_task(&tasks[small++], &tasks[i]);

    swap_task(&tasks[small], &tasks[task_amount - 1]);

    return small;
}

/* Quick sort of tasks based on the parsed check function. */
static void task_sort(task_t **tasks, int task_amount, int (*check)(task_t *, task_t *)) {
    if(task_amount > 0) {
        u32 partition = decide_partition_task(tasks, task_amount, check);

        task_sort(tasks, partition, check);
        task_sort(tasks + partition + 1, task_amount - partition - 1, check);
    }
}

/* On empty list command, we can sort, list and return after this. */
/* Sort buffer with pointers. */
static void sort_and_print(task_t *tasks, int index) {
    int i;
    task_t *task_sorted[TOTAL_TASKS];
    
    for(i = 0; i < index; ++i) task_sorted[i] = &tasks[i];
    task_sort(task_sorted, index, task_strcmp);
    for(i = 0; i < index; ++i) {
        const task_t *task = task_sorted[i];
        printf("%u %s #%d %s\n", DERIVE_ID(tasks, task, task_t) + 1,\
        *task->activity, task->eta, task->desc);
    }
}

static ALWAYS_INLINE void task_try_move_impl(task_t *task, activity_t *activities, activity_t act,\
int id, bool exists_user, int a_index, u32 time) {
    if(!strcmp(act, T_TO_DO)) {
        /* We can't set it to "TO DO" if we have already started. */
        if(strcmp(*task->activity, T_TO_DO) != 0)
            puts(T_TASK_ALREADY_STARTED);
        return;
    }

    if(!exists_user) {
        puts(T_NO_SUCH_USER);
        return;
    }

    if((id = get_activity_index(activities, act, a_index)) == a_index) {
        puts(T_NO_SUCH_ACTIVITY);
        return;
    }

    /* When transitioning from "TO DO", we set the starting time. */
    if(!strcmp(*task->activity, T_TO_DO))
        task->start = time;

    /* When finishing a task. */
    if(!strcmp(act, T_DONE)) {
        if(strcmp(*task->activity, T_DONE)) {
            const int duration = time - task->start, slack = duration - task->eta;
            printf(T_DURATION_SLACK, duration, slack);
        }

        /* DONE to DONE does nothing. */
        else return;
    }

    /* Set to the read activity. */
    task->activity = &activities[id];
}

void task_push(task_t *tasks, activity_t *activities, int *index) {
    /* Out of bounds. */
    if(*index >= TOTAL_TASKS) {
        puts(T_TOO_MANY_TASKS);
        clear_stdin();
    }
    
    else {
        task_t * const task = &tasks[*index];
        char * const desc = task->desc;
        scanf("%d", &task->eta);

        get_after_space(desc, DESC_LEN);

        /* Task starts with "TO DO" activity. */
        task->activity = &activities[0];
        task->start = 0;

        if(task_duplicate_description(tasks, desc, *index)) {
            puts(T_DUPLICATE_DESCRIPTION);
            return;
        }

        if(task->eta <= 0) {
            puts(T_INVALID_DURATION);
            return;
        }

        /* Success means we can increment task index. */
        printf(T_TASK_NUMBER, ++*index);
    }
}

void task_list(task_t *tasks, int index) {
    int id, c;
    bool is_all = true;
    do {
        /* Try to find next number (to allow aribitrary amount of spaces). */
        while(isspace(c = getchar())) {
            /* End. */
			if(c == '\n' || c == EOF) {
				if(is_all) sort_and_print(tasks, index);
                return;
            }
		}

        /* Command was not empty. */
        is_all = false;
		
        /* Reinsert the read char in stdin. */
        /* A downside of allowing an aribitrary amount of spaces. */
		ungetc(c, stdin);

        /* Read current identifier and print info. */
        scanf("%d", &id);

        if(id <= 0 || id > index)
            printf(T_NUMBER_NO_SUCH_TASK, id);

        else {
            const task_t *task = &tasks[id - 1];
            printf("%d %s #%d %s\n", id, *task->activity, task->eta, task->desc);
        }
	} while(!is_all);
}

void time_try_set(u32 *time) {
    int try;
    scanf("%d", &try);

    if(try < 0) {
        puts(T_INVALID_TIME);
        return;
    }
    
    /* Success means we can increment time. */
    *time += (u32)try;

    printf("%d\n", *time);
}

void user_push(user_t *users, int *index) {
    char * const usr = users[*index];

    if(user_try_list_is_empty_stdin(usr, *index, users)) return;

    /* 1st char already read inside user_try_list_is_empty_stdin.*/
    fgets(usr + 1, USER_LEN - 1, stdin);
    usr[strlen(usr) - 1] = '\0';

    if(user_exists(users, usr, *index)) {
        puts(T_USER_ALREADY_EXISTS);
        return;
    }

    /* Out of bounds. */
    if(*index >= TOTAL_USERS) {
        puts(T_TOO_MANY_USERS);
        return;
    }

    /* Success means we can increment user index. */
    ++*index;
}

void
task_try_move(task_t *tasks, activity_t *activities, user_t *users, indexes_t *indexes, u32 time) {
    int id;
    user_t usr;
    activity_t act;
    bool exists_user = true;
    scanf("%d", &id);

    if(id <= 0 || id > indexes->t_index) {
        clear_stdin();
        puts(T_NO_SUCH_TASK);
        return;
    }

    /* Usernames have no spaces. */
    scanf("%s", usr);
    exists_user = user_exists(users, usr, indexes->u_index);

    get_after_space(act, ACTIVITY_LEN);

    task_try_move_impl(&tasks[id-1], activities, act, id, exists_user, indexes->a_index, time);
}

void task_activity_list(task_t *tasks, activity_t *activities, indexes_t *indexes) {
    const int a_index = indexes->a_index;
    activity_t act;

    get_after_space(act, ACTIVITY_LEN);

    if(get_activity_index(activities, act, a_index) != a_index) {
        /* Activity exists! Sort buffer with pointers. */
        task_t *task_sorted[TOTAL_TASKS];
        int i, added_tasks_limit = 0;
        const int t_index = indexes->t_index;

        /* We only get the tasks that match the activity. */
        for(i = 0; i < t_index; ++i)
            if(!strcmp(*tasks[i].activity, act))
                task_sorted[added_tasks_limit++] = &tasks[i];

        task_sort(task_sorted, added_tasks_limit, task_numcmp);

        for(i = 0; i < added_tasks_limit; ++i) {
            const task_t *task = task_sorted[i];
            printf("%u %u %s\n", DERIVE_ID(tasks, task, task_t) + 1, task->start, task->desc);
        }
    }

    else puts(T_NO_SUCH_ACTIVITY);
}

void activity_add_list(activity_t *activities, int *index) {
    char * const act = activities[*index];

    if(activity_try_list_is_empty_stdin(act, *index, activities)) return;

    /* 1st char already read in activity_try_list_is_empty_stdin.*/
    fgets(act + 1, ACTIVITY_LEN - 1, stdin);
    act[strlen(act) - 1] = '\0';

    if(get_activity_index(activities, act, *index) != *index) {
        puts(T_DUPLICATE_ACTIVITY);
        return;
    }

    /* Iterate and look for lowercase. */
    if(has_lowercase(act)) {
        puts(T_INVALID_DESCRIPTION);
        return;
    }

    /* Out of bounds. */
    if(*index >= TOTAL_ACTIVITIES) {
        puts(T_TOO_MANY_ACTIVITIES);
        return;
    }
    
    /* Success means we can increment the activity index. */
    ++*index;
}
