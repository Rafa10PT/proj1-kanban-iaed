#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ALWAYS_INLINE __attribute__((always_inline)) __inline__

/* Types. */
typedef enum {false = 0, true = 1} bool;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long u64;

typedef char s8;
typedef short s16;
typedef int s32;
typedef long s64;

#define TOTAL_USERS 50
/* 2 extra for '\n' (in fgets) and '\0'. */
#define USER_LEN 22
typedef char user_t[USER_LEN];

#define TOTAL_ACTIVITIES 10
/* 2 extra for '\n' (in fgets) and '\0'. */
#define ACTIVITY_LEN 22
typedef char activity_t[ACTIVITY_LEN];

#define TOTAL_TASKS 10000
/* 2 extra for '\n' (in fgets) and '\0'. */
#define DESC_LEN 52
typedef struct {
    activity_t *activity;
    int eta;
    u32 start;
    char desc[DESC_LEN];
} task_t;

typedef struct {
    int t_index; /* Tasks. */
    int a_index; /* Activities. */
    int u_index; /* Users. */
} indexes_t;

/* Text. */
#define T_TO_DO "TO DO"
#define T_IN_PROGRESS "IN PROGRESS"
#define T_DONE "DONE"
#define T_TOO_MANY_TASKS "too many tasks"
#define T_TOO_MANY_USERS "too many users"
#define T_TOO_MANY_ACTIVITIES "too many activities"
#define T_DUPLICATE_DESCRIPTION "duplicate description"
#define T_DUPLICATE_ACTIVITY "duplicate activity"
#define T_TASK_NUMBER "task %d\n"
#define T_NUMBER_NO_SUCH_TASK "%d: no such task\n"
#define T_INVALID_DESCRIPTION "invalid description"
#define T_INVALID_DURATION "invalid duration"
#define T_INVALID_TIME "invalid time"
#define T_USER_ALREADY_EXISTS "user already exists"
#define T_NO_SUCH_TASK "no such task"
#define T_NO_SUCH_USER "no such user"
#define T_NO_SUCH_ACTIVITY "no such activity"
#define T_TASK_ALREADY_STARTED "task already started"
#define T_DURATION_SLACK "duration=%u slack=%d\n"


/* Declaration prototypes. */

/*
 *  This function will get the expected duration of a task from stdin
 *  along with its description from the stdin and push the task
 *  by placing it in tasks[index] and incrementing the index.
 *  Output:
 *  "task <id>", where <id> is the identifier value.
 *  Errors:
 *  "too many tasks" if creating the task exceeds the limit.
 *  "duplicate description" if there is already a task with the same description.
 */
void task_push(task_t *tasks, activity_t *activities, int *index);

/*
 *  Lists tasks using the index arg as upperbound.
 *  With no stdin args, all tasks will be listed alphabetically.
 *  With stdin args, the order of the <id>s is the output order.
 *  Output:
 *  "<id> <activity> #<duration> <description>" for each task, one per line.
 *  Errors:
 *  "<id>: no such task" if the task doesn't exist.
 */
void task_list(task_t *tasks, int index);

/*
 *  Adds a value read from stdin to current time (passed in the time arg).
 *  Output:
 *  "<instant>", where <instant> is the new time value.
 *  Errors:
 *  "invalid time" if the input value is not a decimal integer non-negative.
 */
void time_try_set(u32 *time);

/*
 *  This function will get a username from stdin and pushing
 *  by placing it in users[index] and incrementing the index.
 *  If it doesn't find the user in the stdin, it will list all current users.
 *  Output:
 *  "<user>" for each user, one per line.
 *  Errors:
 *  "user already exists" if there is already a user with the read name.
 *  "too many users" if creating the user exceeds the limit.
 */
void user_push(user_t *users, int *index);

/*
 *  Reads a task id, a user and an activity from stdin.
 *  This will move one task from its current activity to another one.
 *  Output:
 *  "duration=<duration> slack=<slack>" if we moved to "DONE", where
 *  <duration> is the time it took and <slack> is <duration> minus the expected
 *  time.
 *  Errors:
 *  "no such task" if there is no task with the id.
 *  "task already started" if we try to move a task back to "TO DO"
 *  without passing through "DONE".
 *  "no such user" if there is no user with the read name.
 *  "no such activity" if there is no activity with the read name.
 */
void
task_try_move(task_t *tasks, activity_t *activities, user_t *users, indexes_t *indexes, u32 time);

/*
 *  Lists all tasks that belong to an activity read from stdin.
 *  Output:
 *  <id> <start> <description>, for each matched task, for each line,
 *  sorted by crescent order of <start>.
 *  Errors:
 *  "no such activity" if there is no activity with the read name.
 */
void task_activity_list(task_t *tasks, activity_t *activities, indexes_t *indexes);

/*
 *  Reads an activity from stdin.
 *  This will try to push the activity by adding it
 *  to activities[indexes->a_index] and incrementing indexes->a_index.
 *  If no activity is read, the current ones are listed.
 *  Output:
 *  "<activity>", for each activity, one per line.
 *  Errors:
 *  "duplicate activity" if there is already an activity with the read name.
 *  "invalid description" if there are lowercase letters in the name of the
 *  activity.
 *  "too many activities" if creating the activity exceeds the limit.
 */
void activity_add_list(activity_t *activities, int *index);
