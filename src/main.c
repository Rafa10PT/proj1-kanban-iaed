#include "data.h"

/*
 *  This function has the main loop of the program.
 *  It will process the input from stdin and behave according to it
 *  by calling the respective functions.
 *  Any error encountered will stop processing the current line and wait
 *  for the next input.
 */
int main() {
    /* Local variables for kanban. */
    activity_t activities[TOTAL_ACTIVITIES] = {T_TO_DO, T_IN_PROGRESS, T_DONE};
    user_t users[TOTAL_USERS] = {0};
    task_t tasks[TOTAL_TASKS] = {0};
    /* We have 0 tasks, 3 activities and 0 users at start as upperbounds. */
    indexes_t indexes = {0, 3, 0};
    /* Start time at 0. */
    u32 time = 0;
    int cmd;
    
    /* Execute until 'q'. */
    while((cmd = getchar()) != 'q') {
        /* 1st char on stream will be our command. */
        /* We'll make sure to read all the line if we get here again. */

        switch(cmd) {
            case 't':
                task_push(tasks, activities, &indexes.t_index);
                break;
            case 'l':
                task_list(tasks, indexes.t_index);
                break;
            case 'n':
                time_try_set(&time);
                break;
            case 'u':
                user_push(users, &indexes.u_index);
                break;
            case 'm':
                task_try_move(tasks, activities, users, &indexes, time);
                break;
            case 'd':
                task_activity_list(tasks, activities, &indexes);
                break;
            case 'a':
                activity_add_list(activities, &indexes.a_index);
                break;
        }
    }

    return 0;
}
